#!/bin/sh

echo "running php artisan config:cache"
php artisan config:cache 
echo "running php artisan config:clear"
php artisan config:clear

echo "Running the app..."
    /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
